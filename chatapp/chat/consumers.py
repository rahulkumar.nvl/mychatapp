import json
import enum
from typing import Optional, Tuple
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from asgiref.sync import sync_to_async
from django.conf import settings
from .models import Message
TEXT_MAX_LENGTH = getattr(settings, "TEXT_MAX_LENGTH", 65535)
print("hhhhhhhhhhhhh")
class ErrorTypes(enum.IntEnum):
    MessageParsingError = 1
    TextMessageInvalid = 2

ErrorDescription = Tuple[ErrorTypes, str]

class ChatConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
       
        self.room_group_name = 'chat_main'

        #join room
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, code):
        #leave room
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )     

    # Receive message from Websocket
    async def receive(self, text_data=None):
        error: Optional[ErrorDescription] = None
        try:
            data = json.loads(text_data)
            message = data['message']

            await self.save_message(self.user, message)

            # send message to room group
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': message,
                }
            )
        except json.JSONDecodeError as e:
            error = (ErrorTypes.MessageParsingError, f"jsonDecodeError - {e}")


    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message to websocket
        await self.send(text_data=json.dumps({
            "message": message,
        }))

    @sync_to_async
    def save_message(self, user, message):
        Message.objects.create(user=user, content=message)
