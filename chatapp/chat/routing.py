from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path('/ws/chattest/', consumers.ChatConsumer.as_asgi())
]